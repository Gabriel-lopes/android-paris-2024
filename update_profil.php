<?php 

	require_once("modele.class.php");
	$unModele = new Modele("localhost", "paris_2024_241", "root", "");

	if(isset($_REQUEST['iduser']) and isset($_REQUEST['nom']) and isset($_REQUEST['prenom']) 
		and isset($_REQUEST['email']) and isset($_REQUEST['mdp']) and isset($_REQUEST['tel'])) {
		$where = array("iduser"=>$_REQUEST['iduser']);
		$donnees = array("nom"=>$_REQUEST['nom'],
						"prenom"=>$_REQUEST['prenom'],
						"email"=>$_REQUEST['email'],
						"mdp"=>$_REQUEST['mdp'],
						"tel"=>$_REQUEST['tel']);
		$unModele->setTable("user");
		$unModele->update($donnees, $where);
		print("['ok':'1']");
	} else {
		print("[]");
	}

	//Pour tester le code taper l'url suivante : 
	//http://localhost/androidParis2024/update_profil.php?iduser=1&nom=Lopes&prenom=Gabriel&email=g@gmail.com&mdp=abc&tel=06987654321

?>