<?php 

	require_once("modele.class.php");
	$unModele = new Modele("localhost", "paris_2024_241", "root", "");

	if(isset($_REQUEST['iduser']) and isset($_REQUEST['idevent']) and isset($_REQUEST['commentaire'])) {
		
		$dt = date("Y-m-d");

		$donnees = array("dateinscription"=>$dt,
						"statut"=>"en cours",
						"commentaire"=>$_REQUEST['commentaire'],
						"iduser"=>$_REQUEST['iduser'],
						"idevent"=>$_REQUEST['idevent']);
		
		$unModele->setTable("inscription");
		$unModele->insert($donnees);
		print("['ok':'1']");
	} else {
		print("[]");
	}

	//Pour tester le code taper l'url suivante : 
	//http://localhost/androidParis2024/inscription.php?commentaire=rien&iduser=2&idevent=2

?>