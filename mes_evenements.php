<?php 

	require_once("modele.class.php");
	$unModele = new Modele("localhost", "paris_2024_241", "root", "");

	if(isset($_REQUEST['iduser'])) {
		$iduser = $_REQUEST['iduser'];
		$unModele->setTable("events"); //Vue events
		$lesEvents = $unModele->selectAll();
		$tab = array();

		foreach($lesEvents as $unEvent) {
			if($unEvent['iduser'] == $iduser) {
				$ligne["idevenement"] = $unEvent['idevenement'];
				$ligne["designation"] = $unEvent['designation'];
				$ligne["dateevent"] = $unEvent['dateevent'];
				$ligne["heureevent"] = $unEvent['heureevent'];
				$ligne["lieu"] = $unEvent['lieu'];
				$ligne["prix"] = $unEvent['prix'];
				$tab[] = $ligne;
			}
		}
		print(json_encode($tab));
	} else {
		print("[]");
	}

	//Pour tester le code taper l'url suivante : 
	//http://localhost/androidParis2024/verif_connexion.php?iduser=1

?>