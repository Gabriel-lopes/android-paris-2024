<?php

	require_once("modele.class.php");
	$unModele = new Modele("localhost", "paris_2024_241", "root", "");

	if(isset($_REQUEST['email']) and isset($_REQUEST['mdp'])) {
		$where = array("email"=>$_REQUEST['email'],
						"mdp"=>$_REQUEST['mdp']);
		$unModele->setTable("user");
		$unUser = $unModele->selectWhere($where);
		if($unUser == null) {
			print("[]");
		} else {
			$tab = array();
			$tab["iduser"] = $unUser['iduser'];
			$tab["nom"] = $unUser['nom'];
			$tab["prenom"] = $unUser['prenom'];
			$tab["email"] = $unUser['email'];
			$tab["mdp"] = $unUser['mdp'];
			$tab["tel"] = $unUser['tel'];
			print("[". json_encode($tab) ."]");
		}
	} else {
		print("[]");
	}

	//Pour tester le code taper l'url suivante : 
	//http://localhost/androidParis2024/verif_connexion.php?email=g@gmail.com&mdp=abc

?>