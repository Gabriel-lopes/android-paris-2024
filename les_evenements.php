<?php
	
	require_once("modele.class.php");
	$unModele = new Modele("localhost", "paris_2024_241", "root", "");

	$unModele->setTable("evenement");
	$lesEvents = $unModele->selectAll();

	$tab = array(); //Tableau dans lequel vont être stockés les évènements
	foreach($lesEvents as $unEvent) {
		$ligne["idevenement"] = $unEvent['idevenement'];
		$ligne["designation"] = $unEvent['designation'];
		$ligne["dateevent"] = $unEvent['dateevent'];
		$ligne["heureevent"] = $unEvent['heureevent'];
		$ligne["lieu"] = $unEvent['lieu'];
		$ligne["prix"] = $unEvent['prix'];

		$tab[] = $ligne;
	}
	print(json_encode($tab));

?>