DROP DATABASE if exists paris_2024_241;
CREATE DATABASE paris_2024_241;
USE paris_2024_241;

CREATE TABLE user (
	iduser int(3) not null auto_increment,
	nom varchar(50),
	prenom varchar(50),
	email varchar(50),
	mdp varchar(200),
	tel varchar(12),
	primary key(iduser)
);

CREATE TABLE evenement (
	idevenement int(3) not null auto_increment,
	designation varchar(50),
	dateevent date,
	heureevent time,
	lieu varchar(50),
	prix float,
	primary key (idevenement)
);

CREATE TABLE inscription (
	idinscription int(3) not null auto_increment,
	dateinscription date,
	statut enum("validee", "en cours", "annulee"),
	commentaire text,
	iduser int(3) not null,
	idevenement int(3) not null,
	primary key (idinscription),
	foreign key (iduser) references user (iduser),
	foreign key (idevenement) references evenement (idevenement)
);

INSERT INTO user VALUES (null, "Lopes", "Gabriel", "g@gmail.com", "abc", "06987654321"), (null, "Benachir", "Hamza", "h@gmail.com", "abc", "079876543321");

INSERT INTO evenement VALUES (null, "Ceremonie", "2022-04-24", "08:00", "Paris", 100), (null, "Anniversaire", "2022-06-10", "00:00", "Marseille", 1);

INSERT INTO inscription values (null, "2022-03-29", "validee", "Grande cérémonie", 1, 1), (null, "2022-04-02", "en cours", "Anniversaire à Marseille", 2, 2);

CREATE VIEW events AS (
	SELECT e.*, i.iduser
	FROM evenement e, inscription i
	WHERE e.idevenement = i.idevenement
);